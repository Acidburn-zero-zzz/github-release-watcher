# GitHub Release Watcher

We are interested in new releases of some tools. This app provides
a simple GitLab CI workflow that checks given repos for releases published in the past day.

## Usage

This app can be used in 3 ways:

- from the gitlab chat operator `/gitlab gitlab-org/configure/github-release-watcher run check-repos fog/fog-google`
- as a scheduled CI job that posts its output to a dedicated Slack channel
- run locally (requires NodeJS)

```
cd src
npm i
./cli.js --help
```

## Setup

The GitHub Release Watcher is meant to be run as part of GitLab CI.

1. Create a new GitLab repo (or fork the current one)
1. (optional) Set up a scheduled job to run the release watcher at regular intervals. The script expects 2 environment variables, `REPOS` and `SLACK_INCOMING_WEBHOOK` variables.
   - `REPOS` is a comma separated list of GitHub repos to watch (e.g. `knative/serving, knative/eventing, fog/fog-google`)
   - `SLACK_INCOMING_WEBHOOK` is the webhook URL to post to a specific Slack channel. It's really [easy to set up](https://api.slack.com/messaging/webhooks).
1. (optional) Set up [Slack slash command](https://docs.gitlab.com/ee/user/project/integrations/slack_slash_commands.html) to run the release watcher on a per request basis.
